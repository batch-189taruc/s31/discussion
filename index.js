// 1. Import/require http from node.js (http is what allows us to create a server)


let http = require('http')

// 2. Declare a variable asssigned with a number value. This will be the port to be used by the server
let port = 4000

// 3. Use http to create a server and write a header, then return data (like 'Hello World' string)
http.createServer(function(request, response){
	// 4. Write the header for the response specifying its content and status.
	response.writeHead(200, {'Content-Type': 'text/plain'})
	// 5. End the function and return a string with 'Hello World'
	response.end('Hello World')

// 6. Attach a port to the server specifiying where you can access the server (http://localhost:4000/)
}).listen(port)

// 7. Let the terminal/console know that the server is running on which port
console.log(`Server is running at localhost: ${port}`)